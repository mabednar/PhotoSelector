from time import time

def timeit(func):
    def _wrapped(*args, **kw):
        start = time()
        result = func(*args, **kw)
        stop = time()
        print '{} - exec time = {:f}'.format(func.__name__, stop-start)
        return result
    return _wrapped
