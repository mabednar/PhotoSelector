# from main import aaQMainWindow
from main import PhotoSelectorGui
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QFont, QIcon, QPixmap
from PyQt5.QtCore import QCoreApplication, QFile, Qt, QRect, QSize

class GuiUtils(PhotoSelectorGui):

    # def __init__(self):
    #     # super(QMainWindow, self).__init__()
    #     self.statusBar()
    #     self.menubar = self.menuBar()

    def file_menu(self):
        self.fileMenu = self.menubar.addMenu('&File')
        self.fileMenu.addAction(self.exitAction)
        self.fileMenu.addAction(self.openAction)
        self.fileMenu.addAction(self.removeAction)
        self.fileMenu.addAction(self.saveAction)

    def evaluate_menu(self):
        self.evaluateMenu = self.menubar.addMenu('&Evaluation')
        self.evaluateMenu.addAction(self.evaluateAllAction)
        self.evaluateMenu.addAction(self.evaluateSelectedAction)
        self.evaluateMenu.addAction(self.evaluateSerializedAction)
        self.evaluateMenu.addAction(self.evaluateTimelapsAction)

    def selection_menu(self):
        self.selectionMenu = self.menubar.addMenu('&Selection')
        self.selectionMenu.addAction(self.selectAllAction)
        self.selectionMenu.addAction(self.deselectAllAction)
        self.selectionMenu.addAction(self.reversSelectionAction)
        self.selectionMenu.addAction(self.getTimelapsAction)
        self.selectionMenu.addAction(self.getSerializedAction)

    def preferences_menu(self):
        self.preferencesMenu = self.menubar.addMenu('&Preferences')
        self.preferencesMenu.addAction(self.preferencesAction)

    def view_menu(self):
        self.viewMenu = self.menubar.addMenu('&View')
        self.viewMenu.addAction(self.viewAction)
        self.viewMenu.addAction(self.fullScreenAction)

    def menu_actions(self):
        self.openAction = QAction('Open folder', self)
        self.openAction.setShortcut('Ctrl+O')
        self.openAction.setStatusTip('Open Folder')
        self.openAction.triggered.connect(self.open_folder)

        self.exitAction = QAction('Exit', self)
        self.exitAction.setShortcut('Ctrl+Q')
        self.exitAction.setStatusTip('Close PhotoSelector')
        self.exitAction.triggered.connect(self.close)

        self.removeAction = QAction('Remove Selected', self)
        self.removeAction.triggered.connect(self.dummyAction)

        self.saveAction = QAction('Save Selected', self)
        self.saveAction.setShortcut('Ctrl+S')
        self.saveAction.setStatusTip('Save Changes')
        self.saveAction.triggered.connect(self.dummyAction)

        self.evaluateAllAction = QAction('Evaluate All', self)
        self.evaluateAllAction.setShortcut('Ctrl+E')
        self.evaluateAllAction.setStatusTip('Evaluate Photos')
        self.evaluateAllAction.triggered.connect(self.dummyAction)

        self.evaluateSelectedAction = QAction('Evaluate Selected', self)
        self.evaluateSelectedAction.triggered.connect(self.dummyAction)

        self.evaluateTimelapsAction = QAction('Evaluate Timelaps', self)
        self.evaluateTimelapsAction.triggered.connect(self.dummyAction)

        self.evaluateSerializedAction = QAction('Evaluate Serialized', self)
        self.evaluateSerializedAction.triggered.connect(self.dummyAction)

        self.getSerializedAction = QAction('Get Serialized', self)
        self.getSerializedAction.triggered.connect(self.dummyAction)

        self.getTimelapsAction = QAction('Get Timelaps', self)
        self.getTimelapsAction.triggered.connect(self.dummyAction)

        self.selectAllAction = QAction('Select All', self)
        self.selectAllAction.triggered.connect(self.dummyAction)

        self.deselectAllAction = QAction('Deselect All', self)
        self.deselectAllAction.triggered.connect(self.dummyAction)

        self.reversSelectionAction = QAction('Reverse Selection', self)
        self.reversSelectionAction.triggered.connect(self.dummyAction)

        self.viewAction = QAction('View', self)
        self.viewAction.setShortcut('Ctrl+V')
        self.viewAction.setStatusTip('Change View')
        self.viewAction.triggered.connect(self.dummyAction)

        self.fullScreenAction = QAction('Full Screen', self)
        self.fullScreenAction.setShortcut('F11')
        self.fullScreenAction.triggered.connect(self.view_full_screen)

        self.preferencesAction = QAction('Preferences', self)
        self.preferencesAction.setShortcut('Ctrl+P')
        self.preferencesAction.triggered.connect(self.dummyAction)

    def open_folder(self):
        folder_path = QFileDialog.getExistingDirectory(self, 'Open Folder', os.getenv('HOME'), QFileDialog.ShowDirsOnly)
        print 'folder_path = {}'.format(folder_path)
        fh = ''

        if QFile.exists(folder_path):
            fh = QFile(folder_path)

        # if not fh.open(QFile.ReadOnly):
        #     QtGui.qApp.quit()

        data = fh.readAll()
        codec = QTextCodec.codecForUtfText(data)
        unistr = codec.toUnicode(data)

        tmp = ('Notepad: %s' % folder_path)
        self.setWindowTitle(tmp)

        self.textEdit.setText(unistr)

    def evaluate(self):
        pass

    def view_full_screen(self):
        if not self.isFullScreen():
            self.showFullScreen()
        else:
            self.showMaximized()

    def dummyAction(self):
        pass
