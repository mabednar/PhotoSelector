#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
GUI for photo analyzer
author: Marek Bednarek
"""

import sys
import os
import actions
import base64
from time import sleep
from decorator import *
from crawler import Crawler
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QFont, QIcon, QPixmap, QImage, QImageReader
from PyQt5.QtCore import QCoreApplication, QFile, Qt, QRect, QSize

class PhotoSelectorGui(QMainWindow):

    def __init__(self):
        super(QMainWindow, self).__init__()
        self.initUI()

    def initUI(self):
        self.setMinimumSize(800, 600)
        QToolTip.setFont(QFont('SansSerif', 10))
        self.setWindowTitle('PhotoSelector')

        self.showMaximized()
        self.createActions()
        self.createMenus()
        self.createDisplayArea()
        self.open_folder()
        # self.display_picture()
        # self.update()
        # self.createScrollArea()
        # self.imageLabel()



        # print self.geometry().width()
        # print self.height()
        # print self.frameGeometry()

    def createMenus(self):
        self.fileMenu = QMenu('&File', self)
        self.fileMenu.addAction(self.exitAction)
        self.fileMenu.addAction(self.openAction)
        self.fileMenu.addAction(self.removeAction)
        self.fileMenu.addAction(self.saveAction)

        self.evaluateMenu = QMenu('&Evaluation', self)
        self.evaluateMenu.addAction(self.evaluateAllAction)
        self.evaluateMenu.addAction(self.evaluateSelectedAction)
        self.evaluateMenu.addAction(self.evaluateSerializedAction)
        self.evaluateMenu.addAction(self.evaluateTimelapsAction)

        self.selectionMenu = QMenu('&Selection', self)
        self.selectionMenu.addAction(self.selectAllAction)
        self.selectionMenu.addAction(self.deselectAllAction)
        self.selectionMenu.addAction(self.reversSelectionAction)
        self.selectionMenu.addAction(self.getTimelapsAction)
        self.selectionMenu.addAction(self.getSerializedAction)

        self.preferencesMenu = QMenu('&Preferences', self)
        self.preferencesMenu.addAction(self.preferencesAction)

        self.viewMenu = QMenu('&View', self)
        self.viewMenu.addAction(self.viewAction)
        self.viewMenu.addAction(self.fullScreenAction)

        self.menuBar().addMenu(self.fileMenu)
        self.menuBar().addMenu(self.evaluateMenu)
        self.menuBar().addMenu(self.selectionMenu)
        self.menuBar().addMenu(self.preferencesMenu)
        self.menuBar().addMenu(self.viewMenu)

    def createActions(self):
        self.openAction = QAction('Open folder', self)
        self.openAction.setShortcut('Ctrl+O')
        self.openAction.setStatusTip('Open Folder')
        self.openAction.triggered.connect(self.open_folder)

        self.exitAction = QAction('Exit', self)
        self.exitAction.setShortcut('Ctrl+Q')
        self.exitAction.setStatusTip('Close pictureelector')
        self.exitAction.triggered.connect(self.close)

        self.removeAction = QAction('Remove Selected', self)
        self.removeAction.triggered.connect(self.dummy_action)

        self.saveAction = QAction('Save Selected', self)
        self.saveAction.setShortcut('Ctrl+S')
        self.saveAction.setStatusTip('Save Changes')
        self.saveAction.triggered.connect(self.dummy_action)

        self.evaluateAllAction = QAction('Evaluate All', self)
        self.evaluateAllAction.setShortcut('Ctrl+E')
        self.evaluateAllAction.setStatusTip('Evaluate picture')
        self.evaluateAllAction.triggered.connect(self.dummy_action)

        self.evaluateSelectedAction = QAction('Evaluate Selected', self)
        self.evaluateSelectedAction.triggered.connect(self.dummy_action)

        self.evaluateTimelapsAction = QAction('Evaluate Timelaps', self)
        self.evaluateTimelapsAction.triggered.connect(self.dummy_action)

        self.evaluateSerializedAction = QAction('Evaluate Serialized', self)
        self.evaluateSerializedAction.triggered.connect(self.dummy_action)

        self.getSerializedAction = QAction('Get Serialized', self)
        self.getSerializedAction.triggered.connect(self.dummy_action)

        self.getTimelapsAction = QAction('Get Timelaps', self)
        self.getTimelapsAction.triggered.connect(self.dummy_action)

        self.selectAllAction = QAction('Select All', self)
        self.selectAllAction.triggered.connect(self.dummy_action)

        self.deselectAllAction = QAction('Deselect All', self)
        self.deselectAllAction.triggered.connect(self.dummy_action)

        self.reversSelectionAction = QAction('Reverse Selection', self)
        self.reversSelectionAction.triggered.connect(self.dummy_action)

        self.viewAction = QAction('View', self)
        self.viewAction.setShortcut('Ctrl+V')
        self.viewAction.setStatusTip('Change View')
        self.viewAction.triggered.connect(self.dummy_action)

        self.fullScreenAction = QAction('Full Screen', self)
        self.fullScreenAction.setShortcut('F11')
        self.fullScreenAction.triggered.connect(self.view_full_screen)

        self.preferencesAction = QAction('Preferences', self)
        self.preferencesAction.setShortcut('Ctrl+P')
        self.preferencesAction.triggered.connect(self.dummy_action)

    def createDisplayArea(self):
        self.scrollArea = QScrollArea()
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.setCentralWidget(self.scrollArea)
        self.displayArea = QWidget(self.scrollArea)
        self.grid = QGridLayout(self.displayArea)
        self.scrollArea.setWidget(self.displayArea)

    @timeit
    def open_folder(self):
        folder_path = QFileDialog.getExistingDirectory(self, 'Open Folder', os.getenv('HOME'), QFileDialog.ShowDirsOnly)
        if folder_path != "":
            crawler = Crawler(folder_path)
            if 0 == crawler.count_pictures():
                QMessageBox.information(self, 'Empty Folder',
                                        'No pictures with extension {} found'.format(' '.join(Crawler.picture_extentions)))
            else:
                self.display_images(crawler.get_pictutes_paths())

    def load_image(self, file_path):
        ## using QPixmap
        pixmap = QPixmap(file_path).scaled(940, 900, Qt.KeepAspectRatio)
        if pixmap.isNull():
            QMessageBox.information(self, "Image Error", "Cannot load {}.".format(image))
            return
        return pixmap

        # # using QImageReader
        # imageReader = QImageReader(file_path)
        # imageReader.setScaledSize(QSize(940, 900))
        # image = imageReader.read()
        # pixmap = QPixmap.fromImage(image)
        # if pixmap.isNull():
        #     QMessageBox.information(self, "Image Error", "Cannot load {}.".format(image))
        #     return
        # return pixmap

    @timeit
    def display_images(self, files_path):
        images = {}
        row_counter = 0
        for img in enumerate(files_path):
            if not img[0] % 2 and img[0] != 0:
                row_counter += 1
            images[img[0]] = QLabel()
            images[img[0]].resize(940, 900)
            images[img[0]].setPixmap(self.load_image(img[1]))
            self.grid.addWidget(images[img[0]],row_counter, img[0] % 2)

    def evaluate(self):
        pass

    def view_full_screen(self):
        if not self.isFullScreen():
            self.showFullScreen()
        else:
            self.showMaximized()

    def dummy_action(self):
        pass




        # # grid.show()
        # # self.grid.addWidget(QPushButton('button'), 0, 1)
        # # self.grid.addWidget(pic, 1, 1)
        # self.show()


def main():
    app = QApplication(sys.argv)
    gui = PhotoSelectorGui()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()



# https://github.com/baoboa/pyqt5/blob/master/examples/widgets/imageviewer.py
# http://pyqt.sourceforge.net/Docs/PyQt5/
# https://github.com/baoboa/pyqt5/blob/master/examples/widgets/imageviewer.py
# http://www.intransitione.com/blog/pyqt-playground-image-gallery-with-zoom/
