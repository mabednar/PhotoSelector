import os
import exifread
import datetime
import time

class Crawler(object):

    picture_extentions = ('.jpg', '.jpeg')
    timelaps_above = 10
    serial_number = [10]
    picture_folders = []
    picture_names = []

    def __init__(self, target):
        self.target = target

    def count_pictures(self, target=None):
        target = target if target else self.target
        counter = 0
        root, dirs, files = next(os.walk(target))
        for f in files:
            for picture_extention in self.picture_extentions:
                if f.lower().endswith(picture_extention):
                    counter += 1
        return counter

    def get_pictures_names(self, target=None):
        target = target if target else self.target
        picture_names = []
        root, dirs, files = next(os.walk(target))
        for f in files:
            for picture_extention in self.picture_extentions:
                if f.lower().endswith(picture_extention):
                    picture_names.append(f)
        return picture_names

    def get_pictutes_paths(self, target=None):
        target = target if target else self.target
        return [os.path.join(target, f) for f in self.get_pictures_names(target)]

    def get_file_structure(self):
        for root, dirs, files in os.walk(path):
            found = False
            for f in files:
                for picture_extention in self.picture_extentions:
                    if f.lower().endswith(picture_extention):
                        self.picture_folders.append(root)
                        picture_number = self.count_picture(root)
                        print 'Found  {:<4} images in  {}'.format(picture_number, root)
                        found = True
                if found:
                    self.picture_folders.append(root)
                    break

    def _get_time_diff(self, path):
        picture_time_diff = []
        for root, dirs, files in os.walk(path):
            previous_time = 0
            for f in files:
                for picture_extention in self.picture_extentions:
                    if f.lower().endswith(picture_extention):
                        with open(os.path.join(path, f), 'rb') as picture:
                            tags = exifread.process_file(picture, details=False, stop_tag='EXIF DateTimeOriginal')
                            take_time = str(tags['EXIF DateTimeOriginal'])
                            take_time_timestamp = time.mktime(datetime.datetime.strptime(take_time, "%Y:%m:%d %H:%M:%S").timetuple())
                        if previous_time == 0:
                            previous_time = take_time_timestamp
                        time_diff = take_time_timestamp - previous_time
                        previous_time = take_time_timestamp
                        picture_time_diff.append((time_diff, f))
                        print 'picture taken - {:<10} - {}'.format(take_time, time_diff)
        return picture_time_diff

    def _get_serialized(self, path):
        # self.serial_number
        # self.timelaps_above

        serial_picture = []
        timelaps_picture = []
        diff = self._get_time_diff(path)
        sequence_start = False
        # for item in diff:
        #     print item
        for index, item in enumerate(diff):
            if sequence_start:
                if item[0] > 1:
                    if self._is_serial(sequence_start, index):
                        pass
            else:
                sequence_start = index
            # if item[0] > 1 and not start:
            #     start = index
            # if item[0] > 1 and start:
            #     stop = index
            #     start = False
            # for serial in self.serial_number:
            #     if counter == serial:
            #         serial_picture.append(item[1])

    def _is_serial(self, start, stop):
        for serial in self.serial_number:
            if serial == stop - start:
                return True
        return  False

    def get_serial_picture(self, path):
        # print self.serial_number
        pass

    def get_timelaps_picture(self, path):
        # counter = 0
        # diff_info = self._get_time_diff(path)
        # print diff_info
        # for time_diff, file in diff_info:
        self._get_serialized(path)


if __name__ == '__main__':

    path = r'/home/marek/Desktop/TestPhotos'
    album_crawler = Crawler(path)
    # print album_crawler.count_pictures()
    # print album_crawler.get_pictures_names()
    # print album_crawler.get_pictutes_paths()



    # album_crawler.get_timelaps_picture(r'C:\Tajlandia_in_progress\GoPro\1\DCIM\Timelaps\2')
    # album_crawler.get_file_structure()
    # ptimes = album_crawler._get_time_diff(r'C:\Tajlandia_in_progress\GoPro\1\DCIM\Timelaps\1')
