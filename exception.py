class PhotoAutoSelectorError(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class InvalidSource(PhotoAutoSelectorError):
    """
    invalid source file error - lack of file, wrong format, etc
    """
    def __init__(self, msg, expr):
        self.msg = msg
        self.expr = expr

    def __str__(self):
        return repr('{} - {}'.format(self.msg, self.expr))
