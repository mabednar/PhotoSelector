from setuptools import setup, find_packages

setup(
    name='PhotoSelector',
    version='0.0.1',
    description='Photo automatic selection tool',
    url='https://gitlab.com/mabednar/PhotoSelector',
    download_url='https://github.com/Glenpl/openytapi/tarball/2.1.0',
    author='mabednar/Marek Bednarek',
    author_email='marek.bednarekremovethis<at>gmail.com',
    license='MIT',
    keywords='pyqt5 image photo evaluation',
    packages=find_packages(),
    install_requires=[
        'exifread >= 2.1.0',
    ]
)


